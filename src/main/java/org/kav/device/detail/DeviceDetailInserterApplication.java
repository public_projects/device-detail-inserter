package org.kav.device.detail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;


@SpringBootApplication
@ComponentScan( {"org.green.cassandra", "org.kav.device.detail"})
public class DeviceDetailInserterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeviceDetailInserterApplication.class, args);
	}

}
