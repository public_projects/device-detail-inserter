package org.kav.device.detail.main;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import org.green.cassandra.services.core.DeviceDetailService;


@Slf4j
@Service("detail-main")
public class Main implements CommandLineRunner {

    @Autowired
    private DeviceDetailService deviceDetailService;

    @Override
    public void run(String... args) throws Exception {
        this.deviceDetailService.writeToDB("DeviceDetails.json");
    }
}
